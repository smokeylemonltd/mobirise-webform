$(".mbr-form").each(function () {
    $(this).validate({
        submitHandler: function (form) {
            var myForm = $(form);
            var alert = myForm.find('.valid-submission');
            var submitBtn = myForm.find('.btn');
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function (response) {
                    alert.show();
                    $(submitBtn).attr("disabled", "disabled");
                    if ($('.formHolder').length) {
                        $('.formHolder').hide();
                    }
                }
            });
        }
    });
});