

Step 1:

Upload all the contents of this folder to the public_html folder of the website

Step 2:

On the Footer area of the HTML:

Find the line:

```
<script src="assets/formoid/formoid.min.js"></script>
```

**Remove and Replace** with

```
<script src="//cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script src="submission.js"></script>
```

Step 3:

Find the **'action'** of the form such as:

```
<form action="https://mobirise.com/" method="POST" ..
```

Make sure the **action** is set to **'submission.php'** as per:
```
<form action="submission.php" method="POST" ..
```

Step 4:

Make sure the validation section changes from: 

```
<div class="form-row">
    <div hidden="hidden" data-form-alert="" class="alert alert-success col-12">Thanks for filling out the form!</div>
    <div hidden="hidden" data-form-alert-danger="" class="alert alert-danger col-12">Oops...! some problem!</div>
</div>
```

To something like:

```
<div class="form-row valid-submission" style="display:none;">
    <div class="alert alert-success col-12">Thanks for filling out the form!</div>
</div>
```

The main important feature is the class name contains '**valid-submission**' and the style is set to '**display:none**'

Notes:


It is important to realise the following fields are submitted (without modification) please note these **must match the 'name' property of the form input fields**. This is setup for you to add to if you wish.

eg:

```
 <input type="text" name="firstName" placeholder="First Name" ...
```

email

firstName

lastName

name

phone

message

To add additional fields, simply open up the submission.php file and copy an existing variable such as:

```
$email = isset($_POST['email']) ? $_POST['email'] : '';
```

and in the htmlMessage variable;
```
<p>Email Address: <strong>$email</strong>";
```

For example, lets use 'Pets Name' it would loo something like:

```
$email = isset($_POST['email']) ? $_POST['email'] : '';
$petsName = isset($_POST['petsName']) ? $_POST['petsName'] : '';
```
And in the htmlMessage..
```
<p>Email Address: <strong>$email</strong>";
<p>Pets Name: <strong>$petsName</strong>";
```

Assuming 'petsName' was the name property of the input field ala
```
 <input type="text" name="petsName" placeholder="Pets Name" ...
```
