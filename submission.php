<?php
use PHPMailer\PHPMailer\PHPMailer;

/*
 * ==================================================================
 * Execute operations upon form submit (store form data in date.csv).
 * ==================================================================
 */
require_once 'vendor/autoload.php';

if (isset($_POST['email'])) {
    // Collect the form data.
    $submissionDate = date('d/m/Y');
    $subject = 'Contact Form';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $firstName = isset($_POST['firstName']) ? $_POST['firstName'] : '';
    $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : '';
    $name = isset($_POST['name']) ? $_POST['name'] : $firstName . ' ' . $lastName; // if first name and last name separate fields, merge them into the name
    $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
    $message = isset($_POST['message']) ? $_POST['message'] : '';

    $htmlMessage
        = "<p>The following enquiry has been sent:</p>
<p>Name: <strong>$name</strong></p>
<p>Email Address: <strong>$email</strong></p>
<p>Phone: <strong>$phone</strong></p>
<p>Message: <strong>$message</strong></p>";

    //Create a new PHPMailer instance
    $mail = new PHPMailer;
// Set PHPMailer to use the sendmail transport
    $mail->isSendmail();
//Set who the message is to be sent from
    $mail->setFrom('no-reply@emailaddress.com', $name); //no-reply@emailaddress.com
//Set an alternative reply-to address
    $mail->addReplyTo($email, $name);
//Set who the message is to be sent to
    $mail->addAddress('contact@emailaddress.com'); //contact@emailaddress.com
//Set the subject line
    $mail->Subject = $subject;
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
    $mail->msgHTML($htmlMessage);
//Replace the plain text body with one created manually
    $mail->AltBody = strip_tags($htmlMessage);
//send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent!";
    }

}